<?php

namespace Tests\Feature;

use App\Currency;
use App\Customer;
use App\ExchangeRate;
use App\Wallet;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentTest extends TestCase
{
    use RefreshDatabase;

    public function testSuccessDeposit()
    {
        /** @var Customer $customer */
        $customer = factory(Customer::class)->create();
        /** @var Currency $currency */
        $currency = factory(Currency::class)->create();
        /** @var Wallet $wallet */
        $wallet = factory(Wallet::class)->create([
            'currency_id' => $currency->id,
            'customer_id' => $customer->id,
            'balance' => 77,
        ]);
        factory(ExchangeRate::class)
            ->state('today')
            ->create([
                'currency_id' => $currency->id,
                'usd_ratio' => 125
            ]);
        $requestData = [
            'wallet_id' => $wallet->id,
            'amount' => 100,
            'currency_code' => $currency->code
        ];
        $response = $this->json('POST', 'api/wallets/'. $wallet->id .'/deposits', $requestData);
        $response->assertStatus(201);
        $wallet->refresh();
        $this->assertEquals($wallet->balance, 10077);
    }

    public function testSuccessTransfer()
    {
        /** @var Customer $issuer */
        $issuer = factory(Customer::class)->create();
        /** @var Currency $issuerCurrency */
        $issuerCurrency = factory(Currency::class)->create(['code' => 'EUR']);
        /** @var ExchangeRate $issuerER */
        factory(ExchangeRate::class)
            ->state('today')
            ->create([
                'currency_id' => $issuerCurrency->id,
                'usd_ratio' => 125
            ]);
        /** @var Wallet $issuerWallet */
        $issuerWallet = factory(Wallet::class)->create([
            'customer_id' => $issuer->id,
            'currency_id' => $issuerCurrency->id,
            'balance' => 50000,
        ]);


        /** @var Customer $recipient */
        $recipient = factory(Customer::class)->create();
        /** @var Currency $recipientCurrency */
        $recipientCurrency = factory(Currency::class)->create(['code' => 'CNY']);
        /** @var Wallet $recipientWallet */
        /** @var ExchangeRate $issuerER */
        factory(ExchangeRate::class)
            ->state('today')
            ->create([
                'currency_id' => $recipientCurrency->id,
                'usd_ratio' => 14
            ]);
        $recipientWallet = factory(Wallet::class)->create([
            'customer_id' => $recipient->id,
            'currency_id' => $recipientCurrency->id,
            'balance' => 20000,
        ]);

        $requestData = [
            'issuer_wallet_id' => $issuerWallet->id,
            'recipient_wallet_id' => $recipientWallet->id,
            'currency_code' => $issuerCurrency->code,
            'amount' => 100
        ];

        $response = $this->json(
            'POST',
            'api/wallets/' . $issuerWallet->id . '/wallets/' . $recipientWallet->id,
            $requestData
        );
        $response->assertStatus(201);

        $issuerWallet->refresh();
        $this->assertEquals(40000, $issuerWallet->balance);
        $recipientWallet->refresh();
        $this->assertEquals(109286, $recipientWallet->balance);
    }
}
