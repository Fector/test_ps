<?php

namespace Tests\Feature;

use App\Currency;
use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get customers
     */
    public function testGetCustomers()
    {
        factory(Customer::class, 10)->create();
        $response = $this->json('GET', '/api/customers');
        $response->assertStatus(200);
    }

    /**
     * Test success registration
     */
    public function testSuccessRegister()
    {
        /** @var Currency $currency */
        $currency = factory(Currency::class)->create();

        $requestData = [
            'name' => 'Иванов Иван',
            'country' => 'РФ',
            'city' => 'Таганрог',
            'currency_code' => $currency->code
        ];
        $response = $this->json('POST','/api/customers', $requestData);
        //$response->assertExactJson([]);
        $response->assertStatus(201);
    }

    /**
     * Test validation errors
     */
    public function testValidationErrors()
    {
        /** @var Currency $currency */
        $currency = factory(Currency::class)->create();
        $requestData = [
            'country' => 'РФ',
            'city' => 'Таганрог',
            'currency_code' => $currency->code
        ];
        $response = $this->json('POST','/api/customers', $requestData);
        $response->assertStatus(422);

        $requestData = [
            'name' => 'Иванов Иван',
            'city' => 'Таганрог',
            'currency_code' => $currency->code
        ];
        $response = $this->json('POST','/api/customers', $requestData);
        $response->assertStatus(422);

        $requestData = [
            'name' => 'Иванов Иван',
            'country' => 'РФ',
            'currency_code' => $currency->code
        ];
        $response = $this->json('POST','/api/customers', $requestData);
        $response->assertStatus(422);

        $requestData = [
            'name' => 'Иванов Иван',
            'country' => 'РФ',
            'currency_code' => 'Таганрог',
        ];
        $response = $this->json('POST','/api/customers', $requestData);
        $response->assertStatus(422);
    }
}
