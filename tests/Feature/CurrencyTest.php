<?php

namespace Tests\Feature;

use App\Currency;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CurrencyTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test success registration
     */
    public function testStoreExchangeRate()
    {
        /** @var Currency $currency */
        $currency = factory(Currency::class)->create();

        $requestData = [
            'usd_ratio' => '1.10',
            'date' => '2019-07-01',
            'currency_code' => $currency->code
        ];
        $response = $this->json(
            'POST',
            '/api/exchange_rates',
            $requestData
        );
        $response->assertStatus(201);
    }
}
