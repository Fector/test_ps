<?php

namespace Tests\Feature;

use App\Currency;
use App\Customer;
use App\ExchangeRate;
use App\Operation;
use App\Transaction;
use App\Wallet;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReportTest extends TestCase
{
    use RefreshDatabase;

    public function testGetOperations()
    {
        /** @var Customer $customer */
        $customer = factory(Customer::class)->create();
        /** @var Currency $currency */
        $currency = factory(Currency::class)->create();
        /** @var Wallet $wallet */
        $wallet = factory(Wallet::class)->create([
            'currency_id' => $currency->id,
            'customer_id' => $customer->id,
            'balance' => 100000,
        ]);
        factory(ExchangeRate::class)
            ->state('today')
            ->create([
                'currency_id' => $currency->id,
                'usd_ratio' => 125
            ]);

        factory(Transaction::class, 10)
            ->state('today')
            ->create(['currency_id' => $currency->id])
            ->each(function (Transaction $t) use ($wallet) {
                factory(Operation::class)
                    ->state('deposit')
                    ->state('today')
                    ->create([
                        'wallet_id' => $wallet->id,
                        'transaction_id' => $t->id,
                        'amount' => $t->amount,
                        'usd_amount' => 10000
                    ]);
            });

        factory(Transaction::class, 10)
            ->state('today')
            ->create(['currency_id' => $currency->id])
            ->each(function (Transaction $t) use ($wallet) {
                factory(Operation::class)
                    ->state('withdraw')
                    ->state('today')
                    ->create([
                        'wallet_id' => $wallet->id,
                        'transaction_id' => $t->id,
                        'amount' => $t->amount,
                        'usd_amount' => 5000,
                        'is_income' => false,
                    ]);
            });

        $uri = 'api/customers/' . $customer->id . '/operations';
        $requestData = [
            'from_date' => Carbon::now()->subDay()->toDateString(),
            'to_date' => Carbon::now()->addDay()->toDateString(),
        ];
        $response = $this->json('GET', $uri, $requestData);
        $content = json_decode($response->getContent(), true);

        $response->assertStatus(200);
        $this->assertCount(20, $content['data']);
        $this->assertEquals(500, $content['totals']['usd_amount']);
    }
}
