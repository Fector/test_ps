<?php

namespace App\Services;

use Illuminate\Support\Collection;

/**
 * Interface ReportServiceInterface
 * @package App\Services
 */
interface ReportServiceInterface
{
    /**
     * @return Collection
     */
    public function operations(): Collection;

    /**
     * @return float|int
     */
    public function totalAmount();

    /**
     * @return float|int
     */
    public function totalUsdAmount();
}
