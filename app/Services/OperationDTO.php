<?php

namespace App\Services;

use App\Currency;
use App\Wallet;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;

/**
 * Class OperationDTO
 * @package App\Services
 */
class OperationDTO
{
    /**
     * @var Wallet
     */
    protected $wallet;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var int
     */
    protected $baseAmount;

    /**
     * @var int
     */
    protected $usdAmount;

    /**
     * @var bool
     */
    protected $isIncome;

    /**
     * @var Carbon
     */
    protected $date;

    /**
     * @var Order
     */
    protected $order;

    /**
     * OperationDTO constructor.
     * @param Wallet $wallet
     * @param Order $order
     * @param bool $isIncome
     */
    public function __construct(Wallet $wallet, Order $order, bool $isIncome)
    {
        $this->order = $order;
        $this->wallet = $wallet;
        $wallet->isSameCurrency($order->currency());
        $this->date = $order->date();
        $moneyPair = App::makeWith(MoneyPair::class, [
            'amount' => $order->baseAmount(),
            'fromCurrency' => $order->currency(),
            'toCurrency' => $wallet->currency,
            'date' => $this->date
        ]);
        $this->usdAmount = $moneyPair->usdAmount();
        if ($wallet->isSameCurrency($order->currency())) {
            $this->baseAmount = $order->baseAmount();
        } else {
            $this->baseAmount = $moneyPair->toAmount();
        }
        $this->isIncome = $isIncome;
    }

    /**
     * @return Wallet
     */
    public function wallet(): Wallet
    {
        return $this->wallet;
    }

    /**
     * @return Currency
     */
    public function currency(): Currency
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function baseAmount(): int
    {
        return $this->baseAmount;
    }

    /**
     * @return int
     */
    public function usdAmount(): int
    {
        return $this->usdAmount;
    }

    /**
     * @return bool
     */
    public function isIncome(): bool
    {
        return $this->isIncome;
    }
}
