<?php

namespace App\Services;

use App\Currency;
use App\Exceptions\CanNotMakeDeposit;
use App\Exceptions\CanNotMakeTransfer;
use App\Exceptions\CanNotUpdateBalance;
use App\Exceptions\UnauthorizedCurrency;
use App\Operation;
use App\Transaction;
use App\Wallet;
use Illuminate\Support\Facades\DB;

/**
 * Class BrokerService
 * @package App\Services
 */
class BrokerService implements BrokerServiceInterface
{
    /**
     * @param Order $order
     * @param $operationDTO $dto
     * @throws CanNotMakeDeposit
     */
    public function deposit(Order $order, OperationDTO $operationDTO): void
    {
        try {
            DB::beginTransaction();
            $transaction = $this->createTransaction($order);
            $this->createOperation($transaction, $operationDTO);
            $this->addBalance($operationDTO->wallet(), $operationDTO->baseAmount());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new CanNotMakeDeposit($e->getMessage());
        }
    }

    /**
     * @param Order $order
     * @param OperationDTO $issuerOperation
     * @param OperationDTO $recipientOperation
     * @throws CanNotMakeTransfer
     */
    public function transfer(Order $order, OperationDTO $issuerOperation, OperationDTO $recipientOperation): void
    {
        try {
            DB::beginTransaction();
            $this->checkTransferCurrency(
                $order->currency(),
                $issuerOperation->wallet(),
                $recipientOperation->wallet()
            );
            $transaction = $this->createTransaction($order);
            $this->createOperation($transaction, $issuerOperation);
            $this->subBalance($issuerOperation->wallet(), $issuerOperation->baseAmount());
            $this->createOperation($transaction, $recipientOperation);
            $this->addBalance($recipientOperation->wallet(), $recipientOperation->baseAmount());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new CanNotMakeTransfer($e->getMessage());
        }
    }

    /**
     * @param Order $order
     * @return Transaction
     */
    public function createTransaction(Order $order): Transaction
    {
        $transaction = Transaction::create([
            'currency_id' => $order->currency()->id,
            'amount' => $order->baseAmount(),
            'created_at' => $order->date()
        ]);
        return $transaction;
    }

    /**
     * @param Transaction $transaction
     * @param OperationDTO $dto
     * @return Operation
     */
    public function createOperation(Transaction $transaction, OperationDTO $dto): Operation
    {
        /** @var Operation $operation */
        $operation = $transaction->operations()->create([
            'wallet_id' => $dto->wallet()->id,
            'amount' => $dto->baseAmount(),
            'is_income'=> $dto->isIncome(),
            'usd_amount' => $dto->usdAmount(),
            'created_at' => $transaction->created_at,
        ]);
        return $operation;
    }

    /**
     * @param Wallet $wallet
     * @param int $baseAmount
     * @throws CanNotUpdateBalance
     * @throws \App\Exceptions\NotEnoughWalletBalance
     */
    public function subBalance(Wallet $wallet, int $baseAmount): void
    {
        $wallet->subBalance($baseAmount);
        if (!$wallet->save()) {
            throw new CanNotUpdateBalance('Can not update a wallet balance');
        }
    }

    /**
     * @param Wallet $wallet
     * @param int $baseAmount
     * @throws CanNotUpdateBalance
     */
    public function addBalance(Wallet $wallet, int $baseAmount): void
    {
        $wallet->addBalance($baseAmount);
        if (!$wallet->save()) {
            throw new CanNotUpdateBalance('Can not update a wallet balance');
        }
    }

    /**
     * @param Currency $currency
     * @param Wallet $issuerWallet
     * @param Wallet $recipientWallet
     * @throws UnauthorizedCurrency
     */
    public function checkTransferCurrency(Currency $currency, Wallet $issuerWallet, Wallet $recipientWallet): void
    {
        if ($issuerWallet->isSameCurrency($currency) || $recipientWallet->isSameCurrency($currency)) {
            return;
        }
        throw new UnauthorizedCurrency('Unauthorized currency for these wallets');
    }
}
