<?php

namespace App\Services;

use App\Currency;
use App\Exceptions\CanNotCreateExchangeRate;
use App\Exceptions\ExchangeRateAlreadyExist;
use App\ExchangeRate;

/**
 * Interface RatesServiceInterface
 * @package App\Services
 */
interface RatesServiceInterface
{
    /**
     * @param string $code
     * @param string $date
     * @param string $usdRatio
     * @return ExchangeRate
     * @throws CanNotCreateExchangeRate
     * @throws ExchangeRateAlreadyExist
     */
    public function create(string $code, string $date, string $usdRatio): ExchangeRate;
}
