<?php

namespace App\Services;

use App\Currency;
use Illuminate\Support\Carbon;

/**
 * Class Order
 * @package App\Services
 */
class Order
{
    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var int
     */
    protected $baseAmount;

    /**
     * @var Carbon
     */
    protected $date;

    /**
     * Order constructor.
     * @param string $currencyCode
     * @param $amount
     * @param Carbon $date
     */
    public function __construct(string $currencyCode, $amount, Carbon $date)
    {
        $this->currency = Currency::whereCode($currencyCode)->firstOrFail();
        $this->baseAmount = (int)($amount*100);
        $this->date = $date;
    }

    /**
     * @return Currency
     */
    public function currency(): Currency
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function baseAmount(): int
    {
        return $this->baseAmount;
    }

    /**
     * @return Carbon
     */
    public function date(): Carbon
    {
        return $this->date;
    }
}
