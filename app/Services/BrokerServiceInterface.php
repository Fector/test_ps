<?php

namespace App\Services;

use App\Exceptions\CanNotMakeDeposit;
use App\Exceptions\CanNotMakeTransfer;

/**
 * Interface BrokerServiceInterface
 * @package App\Services
 */
interface BrokerServiceInterface
{
    /**
     * @param Order $order
     * @param $operationDTO $dto
     * @throws CanNotMakeDeposit
     */
    public function deposit(Order $order, OperationDTO $operationDTO): void;

    /**
     * @param Order $order
     * @param OperationDTO $issuerOperation
     * @param OperationDTO $recipientOperation
     * @throws CanNotMakeTransfer
     */
    public function transfer(Order $order, OperationDTO $issuerOperation, OperationDTO $recipientOperation): void;
}
