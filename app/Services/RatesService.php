<?php

namespace App\Services;

use App\Currency;
use App\Exceptions\CanNotCreateExchangeRate;
use App\Exceptions\ExchangeRateAlreadyExist;
use App\ExchangeRate;

/**
 * Class RatesService
 * @package App\Services
 */
class RatesService implements RatesServiceInterface
{
    /**
     * @param string $code
     * @param string $date
     * @param string $usdRatio
     * @return ExchangeRate
     * @throws CanNotCreateExchangeRate
     * @throws ExchangeRateAlreadyExist
     */
    public function create(string $code, string $date, string $usdRatio): ExchangeRate
    {
        $currency = Currency::whereCode($code)->first();
        $attributes = [
            'currency_id' => $currency->id,
            'date' => $date,
        ];
        $values = [
            'usd_ratio' => $usdRatio*ExchangeRate::RATIO_MULTIPLICAND
        ];
        $exchangeRate = ExchangeRate::firstOrNew($attributes, $values);
        if ($exchangeRate->exists()) {
            throw new ExchangeRateAlreadyExist('Exchange rate already exists');
        }
        if (!$exchangeRate->save()) {
            throw new CanNotCreateExchangeRate();
        }
        return $exchangeRate;
    }
}
