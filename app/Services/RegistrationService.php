<?php

namespace App\Services;

use App\Currency;
use App\Customer;
use App\Exceptions\CanNotRegisterCustomer;
use Illuminate\Support\Facades\DB;

/**
 * Class RegistrationService
 * @package App\Services
 */
class RegistrationService implements RegistrationServiceInterface
{
    /**
     * @param string $name
     * @param string $country
     * @param string $city
     * @param string $currencyCode
     * @return Customer
     * @throws CanNotRegisterCustomer
     */
    public function register(string $name, string $country, string $city, string $currencyCode): Customer
    {
        try {
            DB::beginTransaction();
            $customer = Customer::create([
                'name' => $name,
                'country' => $country,
                'city' => $city
            ]);
            /** @var Currency $currency */
            $currency = Currency::whereCode($currencyCode)->firstOrFail();
            $customer->wallet()->create([
                'currency_id' => $currency->id,
                'balance' => 0
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new CanNotRegisterCustomer();
        }
        return $customer;
    }
}
