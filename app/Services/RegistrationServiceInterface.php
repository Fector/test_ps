<?php

namespace App\Services;

use App\Customer;
use App\Exceptions\CanNotRegisterCustomer;

/**
 * Interface RegistrationServiceInterface
 * @package App\Services
 */
interface RegistrationServiceInterface
{
    /**
     * @param string $name
     * @param string $country
     * @param string $city
     * @param string $currencyCode
     * @return Customer
     */
    public function register(string $name, string $country, string $city, string $currencyCode): Customer;
}
