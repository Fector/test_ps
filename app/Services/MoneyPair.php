<?php

namespace App\Services;

use App\Currency;
use App\ExchangeRate;
use Illuminate\Support\Carbon;

/**
 * Class MoneyPair
 * @package App\Services
 */
class MoneyPair
{
    /**
     * @var int
     */
    protected $fromAmount;

    /**
     * @var int
     */
    protected $usdAmount;

    /**
     * @var int
     */
    protected $toAmount;

    /**
     * MoneyPair constructor.
     * @param int $amount
     * @param Currency $fromCurrency
     * @param Currency $toCurrency
     * @param Carbon $date
     */
    public function __construct(int $amount, Currency $fromCurrency, Currency $toCurrency, Carbon $date)
    {
        $this->fromAmount = $amount;
        $this->usdAmount = $this->toUsd($amount, $fromCurrency, $date);
        if ($fromCurrency->code === $toCurrency->code) {
            $this->toAmount = $amount;
        } else {
            $this->toAmount = $this->fromUsd($this->usdAmount, $toCurrency, $date);
        }
    }

    /**
     * @return int
     */
    public function fromAmount(): int
    {
        return $this->fromAmount;
    }

    /**
     * @return int
     */
    public function toAmount(): int
    {
        return $this->toAmount;
    }

    /**
     * @return int
     */
    public function usdAmount(): int
    {
        return $this->usdAmount;
    }

    /**
     * @param int $amount
     * @param Currency $currency
     * @param Carbon $date
     * @return int
     */
    protected function toUsd(int $amount, Currency $currency, Carbon $date): int
    {
        /** @var ExchangeRate $exchangeRate */
        $exchangeRate = $currency->exchangeRates()
            ->where('date', $date)
            ->first();
        return $exchangeRate->usd_ratio * $amount / ExchangeRate::RATIO_MULTIPLICAND;
    }

    /**
     * @param int $usdAmount
     * @param Currency $currency
     * @param Carbon $date
     * @return int
     */
    protected function fromUsd(int $usdAmount, Currency $currency, Carbon $date): int
    {
        /** @var ExchangeRate $exchangeRate */
        $exchangeRate = $currency->exchangeRates()
            ->where('date', $date)
            ->first();
        return round($usdAmount/$exchangeRate->usd_ratio, 2)*ExchangeRate::RATIO_MULTIPLICAND;
    }
}
