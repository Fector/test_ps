<?php

namespace App\Services;

use App\Customer;
use App\Operation;
use App\Wallet;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportService
 * @package App\Services
 */
class ReportService implements ReportServiceInterface
{
    const QUERY = 'wallet_id, is_income, sum(amount) as total_amount, sum(usd_amount) as total_usd_amount';

    /**
     * @var Wallet
     */
    protected $wallet;
    /**
     * @var string
     */
    protected $fromDate;

    /**
     * @var string
     */
    protected $toDate;

    /**
     * @var mixed
     */
    protected $deposits;

    /**
     * @var mixed
     */
    protected $withdraws;

    /**
     * ReportService constructor.
     * @param Customer $customer
     * @param string $fromDate
     * @param string $toDate
     */
    public function __construct(Customer $customer, string $fromDate, string $toDate)
    {
        $this->wallet = $customer->wallet;
        $this->fromDate = $fromDate;
        $this->toDate = Carbon::parse($toDate)->addDay()->toDateString();
        $totals = DB::table('operations')
            ->selectRaw(self::QUERY)
            ->where('wallet_id', $customer->wallet->id)
            ->whereBetween('created_at', [$this->fromDate, $this->toDate])
            ->groupBy('wallet_id', 'is_income')
            ->get();
        $this->deposits = $totals->first(function ($value, $key) {
            return $value->is_income === true;
        });

        $this->withdraws = $totals->first(function ($value, $key) {
            return $value->is_income === false;
        });
    }

    /**
     * @return Collection
     */
    public function operations(): Collection
    {
        return Operation::whereWalletId($this->wallet->id)
            ->whereBetween('created_at', [$this->fromDate, $this->toDate])
            ->get();
    }

    /**
     * @return float|int
     */
    public function totalAmount()
    {
        $withdrawsTotalAmount = $this->withdraws->total_amount ?? 0;
        $depositsTotalAmount = $this->deposits->total_amount ?? 0;
        return ($depositsTotalAmount - $withdrawsTotalAmount)/100;
    }

    /**
     * @return float|int
     */
    public function totalUsdAmount()
    {
        $withdrawsTotalAmount = $this->withdraws->total_usd_amount ?? 0;
        $depositsTotalAmount = $this->deposits->total_usd_amount ?? 0;
        return ($depositsTotalAmount - $withdrawsTotalAmount)/100;
    }
}
