<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreExchangeRate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usd_ratio' => 'required|regexp:/^[0-9]+\.[0-9]{5}$/',
            'date' => 'required|date_format:Y-m-d',
            'currency_code' => 'required|exists:currencies,code'
        ];
    }
}
