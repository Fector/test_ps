<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\StoreCustomer;
use App\Http\Resources\CustomerResource;
use App\Services\RegistrationServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

/**
 * Class CustomerController
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CustomerResource::collection(Customer::all());
    }

    /**
     * @param StoreCustomer $request
     * @param RegistrationServiceInterface $service
     * @return JsonResponse
     * @throws \App\Exceptions\CanNotRegisterCustomer
     */
    public function register(StoreCustomer $request, RegistrationServiceInterface $service): JsonResponse
    {
        $service->register(
            $request->input('name'),
            $request->input('country'),
            $request->input('city'),
            $request->input('currency_code')
        );
        return response()->json('', Response::HTTP_CREATED);
    }
}
