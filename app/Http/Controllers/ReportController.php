<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\GetOperations;
use App\Http\Resources\OperationResource;
use App\Operation;
use App\Services\ReportServiceInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\App;
use League\Csv\Writer;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class ReportController
 * @package App\Http\Controllers
 */
class ReportController extends Controller
{
    /**
     * @param GetOperations $request
     * @param Customer $customer
     * @return AnonymousResourceCollection
     */
    public function operations(GetOperations $request, Customer $customer): AnonymousResourceCollection
    {
        /** @var ReportServiceInterface $reportService */
        $reportService = App::makeWith(ReportServiceInterface::class, [
            'customer' => $customer,
            'fromDate' => $request->input('from_date'),
            'toDate' => $request->input('to_date')
        ]);
        return OperationResource::collection($reportService->operations())
            ->additional([
                'totals' => [
                    'amount' => $reportService->totalAmount(),
                    'usd_amount' => $reportService->totalUsdAmount()
                ]
            ]);
    }

    /**
     * @param GetOperations $request
     * @param Customer $customer
     * @return StreamedResponse
     */
    public function report(GetOperations $request, Customer $customer): StreamedResponse
    {
        /** @var ReportServiceInterface $reportService */
        $reportService = App::makeWith(ReportServiceInterface::class, [
            'customer' => $customer,
            'fromDate' => $request->input('from_date'),
            'toDate' => $request->input('to_date')
        ]);
        $operations = $reportService->operations();
        return response()->streamDownload(
            function () use ($operations) {
                $csv = Writer::createFromFileObject(new \SplFileObject('php://output', 'w+'));
                $csv->setDelimiter(';');
                $operations->each(function (Operation $operation) use ($csv) {
                    $csv->insertOne([
                        $operation->created_at,
                        $operation->type,
                        $operation->origin_amount,
                        $operation->wallet->currency->code,
                        $operation->origin_amount,
                        'USD'
                    ]);
                });
                $totals = $operations->reduce(function ($carry, Operation $item) {
                    $carry['amount'] += $item->amount;
                    $carry['usd_amount'] += $item->usd_amount;
                    return $carry;
                }, ['amount'=> 0, 'usd_amount' => 0]);
                $csv->insertOne([
                'total',
                '',
                $totals['amount']/100,
                '',
                $totals['usd_amount']/100
            ]);
            },
            'operations.csv',
            [
                'Content-type' => 'text/csv; charset=utf8'
            ]
        );
    }
}
