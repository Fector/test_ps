<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Services\RatesServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class CurrencyController
 * @package App\Http\Controllers
 */
class CurrencyController extends Controller
{
    /**
     * @var RatesServiceInterface
     */
    protected $ratesService;

    /**
     * CurrencyController constructor.
     * @param RatesServiceInterface $ratesService
     */
    public function __construct(RatesServiceInterface $ratesService)
    {
        $this->ratesService = $ratesService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function storeExchangeRate(Request $request): JsonResponse
    {
        $this->ratesService->create(
            $request->input('currency_code'),
            $request->input('date'),
            $request->input('usd_ratio')
        );
        return response()->json('', Response::HTTP_CREATED);
    }
}
