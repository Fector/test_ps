<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTransaction;
use App\Services\BrokerServiceInterface;
use App\Services\OperationDTO;
use App\Services\Order;
use App\Wallet;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;

/**
 * Class PaymentController
 * @package App\Http\Controllers
 */
class PaymentController extends Controller
{
    /**
     * @var BrokerServiceInterface
     */
    protected $broker;

    /**
     * PaymentController constructor.
     * @param BrokerServiceInterface $broker
     */
    public function __construct(BrokerServiceInterface $broker)
    {
        $this->broker = $broker;
    }

    /**
     * @param StoreTransaction $request
     * @param Wallet $wallet
     * @return JsonResponse
     * @throws \App\Exceptions\CanNotMakeDeposit
     */
    public function deposit(StoreTransaction $request, Wallet $wallet): JsonResponse
    {
        $order = App::makeWith(Order::class, [
            'currencyCode' => $request->input('currency_code'),
            'amount' => $request->input('amount'),
            'date' => Carbon::now()
        ]);
        $operationDTO = App::makeWith(OperationDTO::class, [
            'order' => $order,
            'wallet' => $wallet,
            'isIncome' => true
        ]);
        $this->broker->deposit($order, $operationDTO);
        return response()->json('', Response::HTTP_CREATED);
    }

    /**
     * @param StoreTransaction $request
     * @param Wallet $fromWallet
     * @param Wallet $toWallet
     * @return JsonResponse
     * @throws \App\Exceptions\CanNotMakeTransfer
     */
    public function transfer(StoreTransaction $request, Wallet $fromWallet, Wallet $toWallet): JsonResponse
    {
        $order = App::makeWith(Order::class, [
            'currencyCode' => $request->input('currency_code'),
            'amount' => $request->input('amount'),
            'date' => Carbon::now()
        ]);
        $issuerDTO = App::makeWith(OperationDTO::class, [
            'order' => $order,
            'wallet' => $fromWallet,
            'isIncome' => false,
        ]);
        $recipientDTO = App::makeWith(OperationDTO::class, [
            'order' => $order,
            'wallet' => $toWallet,
            'isIncome' => true
        ]);
        $this->broker->transfer($order, $issuerDTO, $recipientDTO);
        return response()->json('', Response::HTTP_CREATED);
    }
}
