<?php

namespace App\Http\Resources;

use App\Operation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class OperationResource
 * @package App\Http\Resources
 * @mixin Operation
 */
class OperationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'date' => $this->created_at->toDateTimeString(),
            'type' => $this->type,
            'amount' => $this->amount/100,
            'currency' => $this->wallet->currency->code,
            'usd_amount' => $this->usd_amount/100
        ];
    }
}
