<?php

namespace App;

use App\Exceptions\NotEnoughWalletBalance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Wallet
 *
 * @property int $id
 * @property int $customer_id
 * @property int $currency_id
 * @property int $balance
 * @property-read \App\Currency $currency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Operation[] $operations
 * @property-read int|null $operations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereId($value)
 * @mixin \Eloquent
 */
class Wallet extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'customer_id',
        'currency_id',
        'balance'
    ];

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return HasMany
     */
    public function operations(): HasMany
    {
        return $this->hasMany(Operation::class);
    }

    /**
     * @param Currency $currency
     * @return bool
     */
    public function isSameCurrency(Currency $currency): bool
    {
        return $currency->code === $this->currency->code;
    }

    /**
     * @param int $baseAmount
     * @return void
     */
    public function addBalance(int $baseAmount): void
    {
        $this->balance += $baseAmount;
    }

    /**
     * @param int $baseAmount
     * @return void
     * @throws NotEnoughWalletBalance
     */
    public function subBalance(int $baseAmount): void
    {
        if ($this->balance < $baseAmount) {
            throw new NotEnoughWalletBalance('Not enough wallet balance');
        }
        $this->balance -= $baseAmount;
    }
}
