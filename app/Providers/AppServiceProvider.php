<?php

namespace App\Providers;

use App\Services\BrokerService;
use App\Services\BrokerServiceInterface;
use App\Services\RatesService;
use App\Services\RatesServiceInterface;
use App\Services\RegistrationService;
use App\Services\RegistrationServiceInterface;
use App\Services\ReportService;
use App\Services\ReportServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        RegistrationServiceInterface::class => RegistrationService::class,
        RatesServiceInterface::class => RatesService::class,
        BrokerServiceInterface::class => BrokerService::class,
        ReportServiceInterface::class => ReportService::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
