<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Operation
 *
 * @property int $id
 * @property int $wallet_id
 * @property int $transaction_id
 * @property int $amount
 * @property int $usd_amount
 * @property bool $is_income
 * @property \Illuminate\Support\Carbon $created_at
 * @property-read float|int $origin_amount
 * @property-read float|int $origin_usd_amount
 * @property-read string $type
 * @property-read \App\Transaction $transaction
 * @property-read \App\Wallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation whereIsIncome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation whereUsdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Operation whereWalletId($value)
 * @mixin \Eloquent
 */
class Operation extends Model
{
    const DEPOSIT_DESCRIPTION = 'The wallet deposited by %f %s';
    const WITHDRAW_DESCRIPTION = '';
    const REPLENISHMENT_OPERATION = '';

    /**
     * @var array
     */
    protected $fillable = [
        'wallet_id',
        'transaction_id',
        'amount',
        'usd_amount',
        'is_income'
    ];

    protected $casts = [
        'created_at' => 'datetime'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    /**
     * @return BelongsTo
     */
    public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }

    /**
     * @return BelongsTo
     */
    public function transaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class);
    }

    /**
     * @return string
     */
    public function getTypeAttribute(): string
    {
        return $this->is_income ? 'Deposit': 'Withdraw';
    }

    /**
     * @return float|int
     */
    public function getOriginAmountAttribute()
    {
        return $this->amount/100;
    }

    /**
     * @return float|int
     */
    public function getOriginUsdAmountAttribute()
    {
        return  $this->usd_amount/100;
    }
}
