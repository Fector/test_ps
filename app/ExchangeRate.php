<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\ExchangeRate
 *
 * @property int $id
 * @property int $currency_id
 * @property mixed $date
 * @property int $usd_ratio
 * @property-read \App\Currency $currency
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeRate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeRate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeRate today()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeRate whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeRate whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeRate whereUsdRatio($value)
 * @mixin \Eloquent
 */
class ExchangeRate extends Model
{
    const RATIO_MULTIPLICAND = 100;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'date',
        'currency_id',
        'usd_ratio'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
    ];

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeToday(Builder $query)
    {
        return $query->where('date', Carbon::today());
    }
}
