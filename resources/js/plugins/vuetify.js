import Vue from 'vue';
import Vuetify, {
    VApp,
    VContent,
    VContainer,
    VCol,
    VRow,
    VCard,
    VToolbar,
    VToolbarTitle,
    VCardText,
    VForm,
    VSelect,
    VMenu,
    VTextField,
    VDatePicker,
    VBtn,
    VSimpleTable,
    VProgressLinear,
    VSnackbar
}  from 'vuetify/lib';

Vue.use(Vuetify, {
    components: {
        VApp,
        VContent,
        VContainer,
        VCol,
        VRow,
        VCard,
        VToolbar,
        VToolbarTitle,
        VCardText,
        VForm,
        VSelect,
        VMenu,
        VTextField,
        VDatePicker,
        VBtn,
        VSimpleTable,
        VProgressLinear,
        VSnackbar
    }
});

const opts = {

};

export default new Vuetify(opts);
