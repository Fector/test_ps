
## Requirements

PHP7.2

PostgreSQL10

Nodejs12

npm6

---

## Install

1. composer install
2. cp .env.example .env
3. php artisan key:generate
4. php artisan migrate
1. php artisan db:seed
5. npm install
6. npm run watch

### API Routes

[GET]  /api/customers

[POST] /api/customers

    body: {
        'name': 'string',
        'country': 'string',
        'city': 'string',
        'currency_code': 'string 3 chars'
    }
    
[POST] /api/exchange_rates
    
    body: {
        'usd_ratio': 'string|string',
        'date': 'string date Y-m-d',
        'currency_code': 'string 3 chars'
    }
    
[POST] /api/wallets/{wallet}/deposits

    body: {
        'currency_code': 'string 3 chars',
        'amount': 'integer or float',
    }
    
[POST] /api/wallets/{fromWallet}/wallets/{toWallet}

    body: {
        'currency_code': 'string 3 chars',
        'amount': 'integer or float',
    }
    
[GET] /api/customers/{customer}/operations

    body: {
        'from_date': 'string',
        'to_date': 'string',
    }
    
[GET] /api/customers/{customer}/report

    body: {
        'from_date': 'string',
        'to_date': 'string',
    }
