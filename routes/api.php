<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {
    Route::get('customers', 'CustomerController@index');
    Route::post('customers', 'CustomerController@register');
    Route::post('exchange_rates', 'CurrencyController@storeExchangeRate');
    Route::post('wallets/{wallet}/deposits', 'PaymentController@deposit');
    Route::post('wallets/{fromWallet}/wallets/{toWallet}', 'PaymentController@transfer');
    Route::get('customers/{customer}/operations', 'ReportController@operations');
    Route::get('customers/{customer}/report', 'ReportController@report');
});
