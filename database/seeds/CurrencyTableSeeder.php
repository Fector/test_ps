<?php

use Illuminate\Database\Seeder;
use App\Currency;
use Money\Currency as MoneyCurrency;
use Money\Currencies\ISOCurrencies;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = new ISOCurrencies();
        /** @var MoneyCurrency $moneyCurrency */
        foreach ($currencies as $moneyCurrency) {
            $currency = Currency::firstOrNew(['code' => $moneyCurrency->getCode()]);
            if (!$currency->exists) {
                $currency->save();
            }
        }
    }
}
