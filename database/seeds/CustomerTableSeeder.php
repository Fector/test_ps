<?php

use Illuminate\Database\Seeder;
use App\Customer;
use App\Wallet;
use App\Currency;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Customer::class, 50)->create()
            ->each(function (Customer $c) {
                factory(Wallet::class)->create([
                    'customer_id' => $c->id,
                    'currency_id' => Currency::inRandomOrder()->first()
                ]);
            });
    }
}
