<?php

use Illuminate\Database\Seeder;
use App\ExchangeRate;
use App\Currency;
use Illuminate\Support\Carbon;

class ExchangeRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 30; $i++) {
            $date = Carbon::now()->subDays($i);
            Currency::all()->each(function (Currency $c) use ($date) {
                factory(ExchangeRate::class)->create([
                    'currency_id' => $c->id,
                    'date' => $date->toDateString()
                ]);
            });
        }
    }
}
