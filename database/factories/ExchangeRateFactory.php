<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ExchangeRate;
use Illuminate\Support\Carbon;
use Faker\Generator as Faker;

$factory->define(ExchangeRate::class, function (Faker $faker) {
    return [
        'date' => $faker->date(),
        'usd_ratio' => $faker->numberBetween(1, 66500),
    ];
});

$factory->state(ExchangeRate::class, 'today', [
    'date' => Carbon::now()->toDateString()
]);
