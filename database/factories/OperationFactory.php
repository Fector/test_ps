<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Operation;
use Illuminate\Support\Carbon;
use Faker\Generator as Faker;

$factory->define(Operation::class, function (Faker $faker) {
    return [
        'amount' => $faker->numberBetween(100, 200),
        'usd_amount' => $faker->numberBetween(100, 200),
        'created_at' => $faker->date()
    ];
});

$factory->state(Operation::class, 'deposit', [
    'is_income' => true
]);

$factory->state(Operation::class, 'withdraw', [
    'is_income' => false,
]);

$factory->state(Operation::class, 'today', [
    'created_at' => Carbon::now()->toDateString()
]);
