<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Transaction;
use Illuminate\Support\Carbon;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'amount' => $faker->numberBetween(100, 200),
        'created_at' => $faker->date()
    ];
});

$factory->state(Transaction::class, 'today', [
    'created_at' => Carbon::now()->toDateString()
]);
