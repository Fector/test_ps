<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('wallet_id');
            $table->integer('transaction_id');
            $table->integer('amount');
            $table->integer('usd_amount');
            $table->boolean('is_income')->default(true);
            $table->timestamp('created_at');

            $table->unique(['transaction_id', 'wallet_id']);
            $table->index(['wallet_id', 'created_at']);
            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
